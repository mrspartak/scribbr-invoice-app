import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
	{
		path: '/',
		component: () => import('@/views/MainLayout'),
		children: [
			{
				name: 'InvoiceList',
				path: '/',
				component: () => import('@/views/invoice/InvoiceList'),
			},
			{
				name: 'InvoiceAdd',
				path: '/add',
				component: () => import('@/views/invoice/InvoiceAdd'),
			},
		],
	},
];

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes,
});

export default router;
