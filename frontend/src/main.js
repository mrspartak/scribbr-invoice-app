import Vue from 'vue';
import App from './App.vue';
import router from './router';

Vue.config.productionTip = false;

import { $http } from './plugins/axios';
import './plugins/theme';

Vue.prototype.$http = $http;

new Vue({
	router,
	render: (h) => h(App),
}).$mount('#app');
