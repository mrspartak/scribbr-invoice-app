let _tsStart = new Date().getTime();
(async () => {
	const { app, config, log } = await require('./server/')();

	app.listen(config.APP_PORT, () => {
		let startDiff = ((new Date().getTime() - _tsStart) / 1000).toFixed(2);
		setTimeout(() => {
			config.STATE = 'up';
		}, 1000);

		log.info({ message: `Started in ${startDiff} sec | Listening at http://127.0.0.1:${config.APP_PORT}`, startTime: startDiff });
	});
})();
