const crypto = require('crypto');

/* string */
exports.trim = function (string, char) {
	if (char === ']') char = '\\]';
	if (char === '\\') char = '\\\\';
	return string.replace(new RegExp('^[' + char + ']+|[' + char + ']+$', 'g'), '');
};

exports.sanitizePath = function (path) {
	try {
		path = decodeURI(path);
		path = path.trim();
		path = path.replace(/\/+/, '/');
		path = exports.trim(path, '/');
		return exports.trim(path, '\\');
	} catch (e) {
		console.error('sanitizePath', e.message);
		return false;
	}
};

/* time */
exports.now = function () {
	return parseInt(new Date().getTime() / 1000);
};

/* promise */
exports.to = function (promise) {
	return promise
		.then((data) => {
			return [null, data];
		})
		.catch((err) => [err]);
};

/**
 * wraps a promise in a timeout, allowing the promise to reject if not resolve with a specific period of time
 * @param {integer} ms - milliseconds to wait before rejecting promise if not resolved
 * @param {Promise} promise to monitor
 * @example
 *  promiseTimeout(1000, fetch('https://courseof.life/johndoherty.json'))
 *      .then(function(cvData){
 *          alert(cvData);
 *      })
 *      .catch(function(){
 *          alert('request either failed or timed-out');
 *      });
 * @returns {Promise} resolves as normal if not timed-out, otherwise rejects
 */
exports.promiseTimeout = function (ms, promise) {
	return new Promise(function (resolve, reject) {
		// create a timeout to reject promise if not resolved
		var timer = requestTimeout(function () {
			reject(new Error('Promise Timed Out'));
		}, ms);

		promise
			.then(function (res) {
				clearRequestTimeout(timer);
				resolve(res);
			})
			.catch(function (err) {
				clearRequestTimeout(timer);
				reject(err);
			});
	});
};

exports.sleep = function (ms) {
	return new Promise((resolve) => setTimeout(resolve, ms));
};

exports.asyncForEach = async function (array, callback) {
	for (let index = 0; index < array.length; index++) {
		await callback(array[index], index, array);
	}
};

//length - 9 maximum
exports.uniqueID = function (length) {
	return Math.random().toString(36).substr(2, length);
};

exports.uniqueIDv2 = function (length) {
	return new Promise((resolve, reject) => {
		crypto.randomBytes(length, (err, buf) => {
			if (err) return reject(err);
			resolve(buf.toString('hex').substr(0, length));
		});
	});
};

//regex
exports.regExpEscape = function (str) {
	return str.replace(/[-[\]{}()*+!<=:?.\/\\^$|#\s,]/g, '\\$&');
};
