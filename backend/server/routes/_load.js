const glob = require('glob');

module.exports = async function (options) {
	let { __, _, app, log } = options;

	const logger = log.child({ place: 'routes/load' });

	//middleware
	options.mdlwr = {};

	options.sendJson = function (res, body, status = 200) {
		res.status(status).json(body);
	};
	options.sendJsonFail = function (res, error, data = undefined) {
		options.sendJson(res, { success: false, error, data }, 400);
	};

	let modules = glob.sync('**/index.js', {
		cwd: __dirname,
	});
	_.each(modules, (modulePath) => {
		let moduleDirPath = modulePath.replace('index.js', '');

		let routeLogger = log.child({ place: `ROUTE/${moduleDirPath}` });

		logger.debug({ message: `ROUTE /${moduleDirPath} loaded` });
		app.use(`/${moduleDirPath}`, require(`${__dirname}/${modulePath}`)(options, { log: routeLogger, moduleName: moduleDirPath }));
	});

	return app;
};
