const Router = require('express-promise-router');
const router = new Router();

module.exports = function (options) {
	let { sendJson, db } = options;

	router.post('/', (req, res) => {
		db.add_invoice(req.body);

		return sendJson(res, {
			success: 1,
		});
	});

	return router;
};
