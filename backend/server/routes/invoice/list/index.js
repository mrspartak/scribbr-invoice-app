const Router = require('express-promise-router');
const router = new Router();

module.exports = function (options) {
	let { db, sendJson } = options;

	router.get('/', (_req, res) => {
		return sendJson(res, {
			success: true,
			data: db.get_invoices(),
		});
	});

	return router;
};
