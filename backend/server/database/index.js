const fs = require('fs');

let INVOICES = [];

module.exports = async function (options) {
	let { log } = options;

	let LAST_ID = 0;
	const STATUS_OPEN = 'open';
	const STATUS_PAID = 'paid';
	const STATUS_LATE = 'late';

	//load database from file
	const _dbFilePath = './data/db.json';
	INVOICES = _readDbFile();

	function _readDbFile() {
		try {
			let data = fs.readFileSync(_dbFilePath);
			return JSON.parse(data) || [];
		} catch (error) {
			console.log('_readDbFile', error);
			return [];
		}
	}
	function write_db_file() {
		try {
			fs.writeFileSync(_dbFilePath, JSON.stringify(INVOICES));
		} catch (error) {
			console.log('write_db_file', error);
		}
	}

	function _get_last_ID() {
		if (!INVOICES?.length) return 0;
		let lastInvoice = INVOICES[INVOICES.length - 1];
		return lastInvoice.id;
	}

	function add_invoice(data) {
		data.id = _get_last_ID() + 1;
		data.status = STATUS_OPEN;
		INVOICES.push(data);

		//send email

		write_db_file();
		return data.id;
	}

	function get_invoices() {
		return INVOICES;
	}

	function set_status(id, status) {
		let index = INVOICES.findIndex((element) => element.id == id);
		if (index >= 0) {
			INVOICES[index].status = status;
		}
		write_db_file();
	}

	return {
		add_invoice,
		get_invoices,
		write_db_file,
		set_status,
		STATUS_OPEN,
		STATUS_PAID,
		STATUS_LATE,
	};
};
