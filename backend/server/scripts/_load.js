const glob = require('glob');

module.exports = async function (options) {
	let { __, _, app, log } = options;

	const logger = log.child({ place: 'scripts/load' });

	let modules = glob.sync('**/*.js', {
		cwd: __dirname,
	});
	_.each(modules, (modulePath) => {
		if (modulePath.indexOf('_load.js') !== -1) return;

		let scriptName = modulePath.replace('.js', '');
		let scriptLogger = log.child({ place: `SCRIPT/${scriptName}` });

		logger.debug({ message: `SCRIPT ${scriptName} loaded` });
		require(`${__dirname}/${modulePath}`)(options, { log: scriptLogger, scriptName });
	});

	return app;
};
