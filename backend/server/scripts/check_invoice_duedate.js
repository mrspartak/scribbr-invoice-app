module.exports = async function (options, { log }) {
	let { db, dayjs } = options;

	doJob();
	setInterval(doJob, 10 * 1000);

	function doJob() {
		let invoices = db.get_invoices();

		for (invoice of invoices) {
			if (!invoice.dueDate || invoice.status !== db.STATUS_OPEN) continue;
			if (dayjs().diff(dayjs(invoice.dueDate)) > 0) {
				log.info({ message: `Invoice ${invoice.id} is due` });
				db.set_status(invoice.id, db.STATUS_LATE);
			}
		}
	}
};
