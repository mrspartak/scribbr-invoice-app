async function Server(options) {
	require('dotenv').config();

	const express = require('express');
	const app = express();
	const _ = require('lodash');
	const __ = require('../utils/utils');
	const dayjs = require('dayjs');

	/* Config from env */
	const config = {
		ENV: process.env.ENV || 'development',
		DEBUG: process.env.DEBUG || false,

		APP_PORT: process.env.APP_PORT || 2000,
		API_URL: process.env.API_URL || 'http://127.0.0.1:2000',

		ID: (await __.uniqueIDv2(2)).toLocaleUpperCase(),
		TS_START: __.now(),
		STATE: 'loading',
	};
	config.DEFAULT_META = {
		service: 'api',
		taskID: `#${config.ID}`,
		server: process.env.SERVER_HOSTNAME,
	};

	/* Logger */
	const log = require('../utils/log')({
		config,
	});
	const logger = log.child({ place: 'server' });
	logger.debug({ message: 'initialization: config, log' });

	/* Initial modules pool */
	const initModules = { __, _, log, config, dayjs };

	/* Middleware */
	const bodyParser = require('body-parser');
	const cors = require('cors');

	app.set('trust proxy', true);
	app.use(bodyParser.json())
		.use(bodyParser.urlencoded({ extended: true, limit: '20mb' }))
		.use(
			cors({
				origin: '*',
			}),
		);

	app.use((req, res, next) => {
		logger.info({
			breakpoint: 'http.request',
			message: `${req.method} ${req.path}`,
			path: req.path,
			method: req.method,
		});
		next();
	});
	logger.debug({ message: 'initialization: express' });

	/* Database */
	const db = await require('./database/')(initModules);

	/* Whole modules pool */
	_.assign(initModules, { app, db });

	/* Routes modules */
	await require('./routes/_load')(initModules);
	logger.debug({ message: 'initialization: api routes' });

	/* Scripts */
	await require('./scripts/_load')(initModules);

	process.on('uncaughtException', (err) => {
		logger.error({ breakPoint: 'uncaughtException', message: err.message, stack: err.stack });
		setTimeout(() => {
			db.write_db_file();
			process.exit();
		}, 10000);
	});

	return initModules;
}

module.exports = Server;
