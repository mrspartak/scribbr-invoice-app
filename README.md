This app consists of 2 parts: frontend written in Vue@v2 and a backend written in NodeJS using express library.

There are 2 options of how to run the app:

1. Cloning and running in local development environment. Of course you can also build a Vue app, but then you have to serve static files via Nginx or serve library for example.

```
1. Clone the repo
//next steps should be done in each directory
2. cd to each directory
3. npm i
4. npm run dev
//Done
5. Open http://localhost:2001/
```

2. Docker images. There is a gitlab pipeline to build 2 images  
   Images can be found here https://gitlab.com/mrspartak/scribbr-invoice-app/container_registry
   The problem is that Vue app has built in API url for production
